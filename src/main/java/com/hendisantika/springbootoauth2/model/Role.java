package com.hendisantika.springbootoauth2.model;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/12/17
 * Time: 07.02
 * To change this template use File | Settings | File Templates.
 */
public enum Role {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_REGISTER,
    ROLE_TRUSTED_CLIENT,
    ROLE_CLIENT
}
