package com.hendisantika.springbootoauth2.repository;

import com.hendisantika.springbootoauth2.model.TokenBlackList;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/12/17
 * Time: 07.09
 * To change this template use File | Settings | File Templates.
 */
public interface TokenBlackListRepo extends Repository<TokenBlackList, Long> {
    Optional<TokenBlackList> findByJti(String jti);

    List<TokenBlackList> queryAllByUserIdAndIsBlackListedTrue(Long userId);

    void save(TokenBlackList tokenBlackList);

    List<TokenBlackList> deleteAllByUserIdAndExpiresBefore(Long userId, Long date);
}
