package com.hendisantika.springbootoauth2.repository;

import com.hendisantika.springbootoauth2.model.Account;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/12/17
 * Time: 07.08
 * To change this template use File | Settings | File Templates.
 */
public interface AccountRepo extends CrudRepository<Account, Long> {
    Optional<Account> findByUsername(String username);

    Account save(Account account);

    int deleteAccountById(Long id);
}
